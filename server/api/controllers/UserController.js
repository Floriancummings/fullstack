/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const jwt = require('jsonwebtoken');

module.exports = {
	create:function(req, res) {
		const user = {
			email: req.param('email'),
			username: req.param('username'),
			password: req.param('password'),
			admin:false
		};
		User.create(user).exec(function(err, createdUser) {
			if (err) {
				if(err.invalidAttributes && err.invalidAttributes.email && err.invalidAttributes.email[0]
				&& err.invalidAttributes.email[0].rule === 'email') return res.badRequest('invalid email address')
				if(err.invalidAttributes && err.invalidAttributes.email && err.invalidAttributes.email[0]
				&& err.invalidAttributes.email[0].rule === 'unique') return res.json({
					success:false,
					type:'email',
				message:{
					type:'email',
					msg:'Email taken by another user'
				}})
				if(err.invalidAttributes && err.invalidAttributes.username && err.invalidAttributes.username[0]
				&& err.invalidAttributes.username[0].rule === 'unique') return res.json({
					success:false,
					type:'username',message:{
						type:'username',
						msg:'Username taken by another user'
					}})
				return res.negotiate(err);
			}
			delete createdUser.password;
			const token = jwt.sign({
  					id:createdUser.id
					}, sails.config.jwt.secret, { expiresIn: 60 });
					return res.json({success:true,user:createdUser,token: token})
		});
	},
	readOne:function(req, res) {

	    User.findOne({id:req.param('id')}).exec(function foundUser(err, user) {

	      if (err) return res.negotiate(err);

	      if (!user) return res.notFound();

	      const data={
	      	username:user.username,
	      	email:user.email,
	      	id:user.id,
	      	admin:user.admin
	      }
	      return res.json(data);
	    });
  	},
	list:function (req,res) {
		User.find().exec(function (err,users) {
			if(err) return res.negotiate(err);
			return res.json(users);
		})
	},
	update:function (req,res) {
		User.update({
			id: req.param('id')
		},
			req.allParams()
		).exec(function(err, updatedUser) {
			return res.json(updatedUser);
		});
	},
	delete:function (req,res) {
		User.destroy({
			id: req.param('id')
		}).exec(function(err, delUser) {
			if(err) return res.negotiate(err)
		});
	},
	login:function (req,res) {
		User.findOne({
			username:req.param('username')
		})
		.exec(function (err,user) {
			if(err) return res.negotiate(err);
			if(user){
				const data ={
					username:user.username,
					email: user.email,
					id: user.id,
					admin: user.admin
				}
				if(user.comparePassword(req.param('password'))){
					const token = jwt.sign({
  					id:user.id
					}, sails.config.jwt.secret, { expiresIn: 60 });
					return res.json({success:true,user:data,token: token})
				}
				else{
					return res.json({
						success:false,
						message:{
							type:'passErr',
							msg:'invalid username or password'
						}})
				}
			}
			else{
				return res.json({success:false,
					message:{
						type:'passErr',
						msg:'invalid username or password'
					}})
			}

		})
	}
};


