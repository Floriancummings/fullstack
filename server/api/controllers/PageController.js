/**
 * PageController
 *
 * @description :: Server-side logic for managing pages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const jwt= require('jsonwebtoken');
module.exports = {
	adminPage: function(req,res) {
		jwt.verify( req.param('token'),sails.config.jwt.secret, 
			function(err, decoded) {
  			if (err) return res.json({success:false,err:err})
  			 User.findOne({id:decoded.id})
  			.exec(function (err, user) {

		      if (err) return res.json({success:false,err:err})

		      if (!user) return res.json({success:false})

		      return res.json({success:true});
	    });
		});

	},
	editPage: function() {


	},
	loginPage:function(req,res) {
		jwt.verify( req.param('token'),sails.config.jwt.secret, 
			function(err, decoded) {
  			if (err) return res.json({success:true,err:err})
  			 User.findOne({id:decoded.id})
  			.exec(function (err, user) {

		      if (err) return res.json({success:true,err:err})

		      if (!user) return res.json({success:true})

		      return res.json({success:false});
	    });
		});
	},
	signUpPage:function(req,res) {
		jwt.verify( req.param('token'),sails.config.jwt.secret, 
			function(err, decoded) {
  			if (err) return res.json({success:true,err:err})
  			 User.findOne({id:decoded.id})
  			.exec(function (err, user) {

		      if (err) return res.json({success:true,err:err})

		      if (!user) return res.json({success:true})

		      return res.json({success:false});
	    });
		});
	},
	profilePage:function(req,res) {
		jwt.verify( req.param('token'),sails.config.jwt.secret, 
			function(err, decoded) {
  			if (err) return res.json({success:false,err:err})
  			 User.findOne({id:decoded.id})
  			.exec(function (err, user) {

		      if (err) return res.json({success:false,err:err})

		      if (!user) return res.json({success:false})
		      	const data={
	      	username:user.username,
	      	email:user.email,
	      	id:user.id,
	      	admin:user.admin
	      }
		      return res.json({success:true,user:data});
	    });
		});
	},

};

