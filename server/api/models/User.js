/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
const bcrypt = require('bcrypt-nodejs');
module.exports = {

  attributes: {
    username:{
      type:'string',
      required:true,
      unique: true

    },
    email:{
      type:'string',
      required:true,
      unique: true,
      email:true

    },
    password:{
      type:'string',
      required:true
    },
    admin:{
      type:'boolean',
    },
    comparePassword:function(password){
      return bcrypt.compareSync(password, this.password);
    }
  },
  beforeCreate: function (values, cb) {

    // Hash password
    bcrypt.hash(values.password, null,null, function(err, hash) {
      if(err) return cb(err);
      values.password = hash;
      cb();
    });
  }
};
