const request =  require('supertest')('http://localhost:1337/')

describe('User controller:: ', function(){
  it('should create a user', function(done) {
   const req = request.post('user/create');
   req.send({
     username:'test3',
     email:'ttt@gmail.com',
     password:'1234'
   })
   req.end(function (err,res) {
     if(err) throw err
     console.log(res.text);
     done();
   })
  }),
  it('should list all users', function(done) {
   const req = request.get('user/list');

   req.end(function (err,res) {
     if(err) throw err
     console.log(res.text);
     done();
   })
  }),

  it('should delete a user', function(done) {
   const req = request.post('user/delete');
   req.send({
     username:'test1'
   })
   req.end(function (err,res) {
     if(err) throw err
     console.log(res.text);
     done();
   })
 }),
 it('should login a user', function(done) {
  const req = request.post('user/login');
  req.send({
    username:'test3',
    password:'1234'
  })
  req.end(function (err,res) {
    if(err) throw err
    console.log(res.text);
    done();
  })
}),
it('should readOne  user', function(done) {
 const req = request.post('user/readOne');
 req.send({
   username:'test3'
 })
 req.end(function (err,res) {
   if(err) throw err
   console.log(res.text);
   done();
 })
})

});
