/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {
	const user = {
        email: 'test1@gmail.com',
        username: 'test1',
        password: '1111',
        admin: true
      };
      User.create(user).exec(function(err, createdUser) {
        if (err) {
          return cb(err)
        }
        console.log(createdUser);
        return cb()
      });
  
};
