import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { Redirect,withRouter,Switch,Route,Link } from 'react-router-dom';
import Edit from './Edit';
import {profilePage,showProfile} from '../actions'
class Profile extends Component{
    componentWillMount(){
    fetch('/page/profilePage',{
      method:'POST',
      body:JSON.stringify({
        token: localStorage.getItem('token')


      })
    }).then(res=>res.json())
    .then(data=>{
      console.log(data)

      if(data.success){
        this.props.showProfile(data.user);
        
      }
      this.props.profilePage()

    })
  }

  render(){
    return(
    
    this.props.profile.receivingData ?<h1>loding...</h1>
    

    : !this.props.profile.showProfilePage ?<Redirect to='/' />:
       <Switch>
        <Route exact path={this.props.match.url} render={()=>( 
          <div className='profile'>
        <h3>{this.props.profile.user.username}</h3>
        <h5>{this.props.profile.user.email}</h5><br/>
        <p>
         <Link to={`${this.props.match.url}/edit/${this.props.profile.user.id}`}  > 
         <button type="button" className="btn btn-success">Edit</button></Link>
        </p>
      </div>)}/>
        <Route path={`${this.props.match.url}/edit/:id`} component={Edit}/>
    </Switch>
   
  

  )
   

  }

}
const mapStateToProps = state =>({
  profile:state.user,
})
const mapDispatchToProps= dispatch=>bindActionCreators({profilePage,showProfile},dispatch)
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Profile));
