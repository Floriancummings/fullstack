import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {editting,handleChange} from '../actions';
import { Redirect } from 'react-router-dom';
import TextField from '../components//TextField';
import Submit from '../components/Submit';

class Edit extends React.Component {
	 componentDidMount(){

    fetch(`/user/readOne/${this.props.match.params.id}`)
    .then((res)=>res.json())
    .then(json=>this.props.editting(json))
  }

handleSubmit=(e)=>{
	e.preventDefault()
	fetch(`/user/readOne/${this.props.match.params.id}`)
    .then((res)=>res.json())
    .then(json=>this.props.editting(''))
}

  render() {
    return (
    	this.props.edit.editting?
    	<Redirect to='/profile' />:
      <form onSubmit={this.handleSubmit}>
       <TextField
        value={this.props.edit.username}
        handleChange={this.props.handleChange}
        type="text"
        name="username"
        placeholder="Username"
        title='Username'
        />
        <TextField
        value={this.props.edit.email}
        handleChange={this.props.handleChange}
        type="email"
        name="email"
        placeholder="Email"
        title='Email'
        />
       <TextField
         value={this.props.edit.password}
         handleChange={this.props.handleChange}
         type="password"
         name="password"
         placeholder="Password"
         title='Password'
        />
      <Submit title='Save' />
    </form>
    );
  }
}
const mapStateToProps = state =>({
  edit: state.edit
})
const mapDispatchToProps = dispatch =>bindActionCreators({editting,handleChange},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(Edit)