import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {showAdmin,adminPage,receiveData, Delete} from '../actions';
import Table from '../components/Table';
import User from './User';
import {
  Link,
  Switch,
  Route,
  withRouter,
  Redirect
} from 'react-router-dom'
class Admin extends Component{
   componentWillMount(){
   
    fetch('/page/afminPage',{
      method:'POST',
      body:JSON.stringify({
        token: localStorage.getItem('token')


      })
    }).then(res=>res.json())
    .then(data=>{
     
      if(data.success){
        this.props.showAdmin();
       
        
      }
       this.props.adminPage()
    })
  }

  componentDidMount(){
    fetch('/user/list')
    .then(res=>res.json())
    .then(users=>{
      setTimeout(()=>{
        this.props.receiveData(users)
      },1500)
      
    })
  }
  handleClick=(e,user)=>{
    e.preventDefault();


      fetch(`/user/delete/${user.id}`)
      this.props.Delete(user)

  }
 render(){

  return(

    this.props.admin.receivingData
    ? <h1>Loading...</h1>: !this.props.admin.shwoAdminPage?<Redirect to='/' />
    :<Switch>
        <Route exact path={this.props.match.url} render={()=>(
          <Table click={this.handleClick} users={this.props.admin.list}/>
        )}/>
        <Route path={`${this.props.match.url}/:id`} component={User}/>

     </Switch>
  )
 }
}



const mapStateToProps = state =>({

  admin:state.admin
})
const mapDispatchToProps = dispatch => bindActionCreators({showAdmin,adminPage,receiveData,Delete},dispatch)
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Admin));
