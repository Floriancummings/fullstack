import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { Redirect,withRouter } from 'react-router-dom';
import {viewUser} from '../actions';

class User extends Component{
  componentDidMount(){

    fetch(`/user/readOne/${this.props.match.params.id}`)
    .then((res)=>res.json())
    .then(json=>this.props.viewUser(json))
  }
  render(){

    return (
      this.props.admin.user &&
      <div className='profile'>
        <h2>{this.props.admin.user.username}</h2>
        <h2>{this.props.admin.user.email}</h2><br/>
        <small>{this.props.admin.user.id}</small>
      </div>

    )
  }


}
const mapStateToProps = state =>({
  admin:state.admin
})
const mapDispatchToProps = dispatch => bindActionCreators({viewUser},dispatch);
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(User));
