import React, { Component } from 'react';
import UserLoggedIn from '../components/UserLoggedIn';
import NotLoggeIn from '../components/NotLoggeIn';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { NavLink } from 'react-router-dom'
import { logout } from '../actions'
class Navbar extends Component{

  render(){
  return (
    <nav className="navbar navbar-default navbar-fixed-top">
      <div className="container">
        <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <NavLink exact activeClassName='active' to='/' className="navbar-brand" >App</NavLink>
        </div>
        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul className="nav navbar-nav">
          {this.props.profile.user ?
            <li><NavLink isActive={this.isActiveFunc}
             activeClassName='active' to={this.props.profile.user.admin ? '/admin':`/profile`}>{this.props.profile.user.admin? 'Admin':'User'}</NavLink></li>:''}
          
        </ul>
          {this.props.profile.user ? <UserLoggedIn click={(e)=>{ e.preventDefault()
            this.props.logout()}} user={this.props.profile.user.username} /> : <NotLoggeIn />}

        </div>
      </div>
    </nav>

  )
}

}
const mapStateToProps=(state)=>({
  profile:state.user
})
const mapDispatchToProps = dispatch =>bindActionCreators({logout},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(Navbar);
