import React, {Component} from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import {Link, Redirect} from 'react-router-dom';
import {showReg, signUpPage,handleChange,registered,onLoad,onError} from '../actions';
import SignUpForm from '../components/SignUpForm';

class SignUp extends Component{
  componentWillMount(){
    fetch('/page/signUpPage',{
      method:'POST',
      body:JSON.stringify({
        token: localStorage.getItem('token')


      })
    }).then(res=>res.json())
    .then(data=>{
      console.log(data)
      if(data.success){
        this.props.showReg();
       
        
      }
       this.props.signUpPage()
    })
  }
  handleSubmit=(e)=>{
    e.preventDefault();
    this.props.onError('');
    const LoginButton = e.target.button;
    LoginButton.disabled = true;
    LoginButton.textContent = 'Creating...'
    fetch('/user/create',{
      method:'POST',
      body: JSON.stringify({
        username:this.props.regdetails.username,
        email:this.props.regdetails.email,
        password:this.props.regdetails.password
      })
    }).then(res=>res.json())
    .then((data)=>{
      if(data.success){
        this.props.registered(data.token);
        localStorage.setItem('token',data.token)
      }else{
        this.props.onError(data.message);
        LoginButton.disabled = '';
        LoginButton.textContent = 'CREATE AN ACCOUNT'
      }
    })}
  render(){
    return(
      this.props.regdetails.receivingData ? <h1>Loading...</h1>:
      !this.props.regdetails.showRegPage?
      <Redirect to='/profile' />:
      <SignUpForm 
      handleSubmit={this.handleSubmit}
      handleChange={this.props.handleChange}
      regdetails={this.props.regdetails}
      />
      
    )
  }
}
const mapStateToProps = state =>({
  regdetails: state.reg
})
const mapDispatchToProps = dispatch =>bindActionCreators({signUpPage,showReg,handleChange,registered,onLoad,onError},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(SignUp)
