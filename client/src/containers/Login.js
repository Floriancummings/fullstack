import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginPage, showLogin,logout,handleChange, loggedIn, onLoad,onError } from '../actions';
import { Redirect } from 'react-router-dom';
import LoginForm from '../components/LoginForm';
import {
  Link
} from 'react-router-dom'
class Login extends Component{
  
  componentWillMount(){
   
    fetch('/page/loginPage',{
      method:'POST',
      body:JSON.stringify({
        token: localStorage.getItem('token')


      })
    }).then(res=>res.json())
    .then(data=>{
     
      if(data.success){
        this.props.showLogin();
       
        
      }
       this.props.loginPage()
    })
  }

  handleSubmit=(e)=>{
    e.preventDefault();
    this.props.onError('')
    const LoginButton = e.target.button;
    LoginButton.disabled = true;
    LoginButton.textContent = 'Processing...'
    fetch('/user/login',{
      method:'POST',
      body: JSON.stringify({
        username: this.props.loginDetails.username,
        password: this.props.loginDetails.password
      })
    }).then((res)=>res.json())
    .then(data=>{
      if(data.success){
        
        setTimeout( () => {
          this.props.loggedIn(data.user)
          localStorage.setItem('token',data.token)
        },1500)

      }
      else {
        this.props.onError(data.message)
        LoginButton.disabled = '';
        LoginButton.textContent = 'Login'
      }
    }).catch((err)=>{
      console.log(err);
      LoginButton.disabled = '';
      LoginButton.textContent = 'Login'
    })}

 render(){
  
    const { loginDetails,handleChange} = this.props;
      return( this.props.loginDetails.receivingData
        ? <h1>Loading...</h1> : !this.props.loginDetails.showLoginPage ?
        <Redirect to={this.props.profile.user&&this.props.profile.user.admin?'/admin':'/profile'} />:
    <LoginForm 
    loginDetails={loginDetails} 
    handleChange={handleChange}
    handleSubmit={this.handleSubmit}
    />
    )
 
  }

}

const mapStateToProps = state =>({
  loginDetails:state.login,
  profile:state.user
})
const mapDispatchToProps = dispatch => bindActionCreators({handleChange,loginPage,logout,showLogin,loggedIn,onLoad,onError},dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Login);
