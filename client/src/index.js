import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers';
import {
  BrowserRouter as Router,
} from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';

const store = createStore(reducers)
ReactDOM.render(
  <Provider store={store}>
    <Router>
    <App />
    </Router>
  </Provider> ,
  document.getElementById('root')
);
registerServiceWorker();
