import {combineReducers} from 'redux';
import user from './reducer-user';
import login from './reducer-login';
import reg from './reducer-reg';
import admin from './reducer-admin';
import edit from './reducers-edit';
const reducers = combineReducers({
  user,
  login,
  reg,
  admin,
  edit
})


export default reducers;
