export default function (state={
  username:'',
  email:'',
  password:'',
  receivingData:true,
  msg:'',
  showRegPage:false
},action) {
  switch (action.type) {
    case 'ON_CHANGE':
      return Object.assign({},state,{[action.payload.name]:action.payload.value})
    case 'ERROR':
       return Object.assign({},state,{msg:action.payload})
    
    case 'REGISTERED':

     return Object.assign({
     email:'',
     password:'',msg:''},{showRegPage:false,registered:true});

     case 'REG_PAGE':
      return Object.assign({},state,{receivingData:false});
    case 'SHOW_REG':
     return Object.assign({},
        state,{showRegPage:true})
    default:
    return state

  }

}
