export default function (state={
  list:[],
  user:null,
  showAdminPage:false,
  receivingData:true
},action) {
  switch (action.type) {
     case 'ADMIN_PAGE':
      return Object.assign({},state,{receivingData:false});
case 'SHOW_ADMIN':
     return Object.assign({},
        state,{showAdminPage:true})
    case 'RECEIVE_DATA':
      return Object.assign({},state,{list:action.payload});
      break;
      case 'VIEW':
       return Object.assign({},state,{user:action.payload})
      break;
    case 'DELETE':
      let id;
      for (let i = 0; i < state.list.length; i++) {
        if(state.list[i].id === action.payload.id){
          id = i
          break;
        }
      }
      
      const li = [...state.list.slice(0,id),
      ...state.list.slice(id+1)]

      return Object.assign({},state,{list:li })
      break;
    default:
    return state

  }
}
