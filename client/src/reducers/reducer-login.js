export default function(state={
  username:'',
  password:'',
  isLoggedIn:false,
  showLoginPage:false,
  msg:'',
  receivingData:true


},action){

  switch (action.type) {
     case 'LOGIN_PAGE':
      return Object.assign({},state,{receivingData:false});
    case 'ON_CHANGE':
      return Object.assign({},
        state,{[action.payload.name]:action.payload.value})
    case 'SHOW_LOGIN':
     return Object.assign({},
        state,{showLoginPage:true})
    case 'LOGGED_IN':
     
     return Object.assign({},
        state,{
       username:'',
       password:'',
       isLoggedIn:true,
        showLoginPage:false,
       msg:''
     })
     break;
     case 'ERROR':
      return Object.assign({},state,{msg:action.payload})
      break;
     default:
      return state

  }

}
