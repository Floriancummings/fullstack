export default function(state={
	user:null,
	showProfilePage:false,
	receivingData:true
}
	,action){

  switch (action.type) {
  	case 'PROFILE_PAGE':
      return Object.assign({},state,{receivingData:false});
    case 'SHOW_PROFILE':
    	return Object.assign({},state,{user:action.payload,
    		showProfilePage:true});
   case 'LOGGED_IN':
   		return Object.assign({},state,{user:action.payload});
   	case 'LOGGED_OUT':
   	localStorage.setItem('token','')
   		return Object.assign({},state,{showProfilePage:false,user:null});
    default:
      return state

  }

}
