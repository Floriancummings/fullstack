import React from 'react'

import reducers from './reducers';
import SignUp from './containers/SignUp';
import { createStore } from 'redux';
import {registered} from './actions'
import renderer from 'react-test-renderer';
import Shallow from 'react-test-renderer/shallow';
import supertest from 'supertest';
const store = createStore(reducers);
const request = supertest('http://localhost:1337/')
describe('Register ',()=>{
	it('should register a user in',(done)=>{
		const req = request.post('user/create')
		req.send({
			username:'test3',
			password:'1111',
			email:'f1@gmail.com',
			admin:false
		})
		req.end(function (err,res) {
			if(err) return console.log(err)
			store.dispatch(registered());
			expect(store.getState().reg.showRegPage)
			.toEqual(false)
			return done()
		})	
			  
	}),
	it('should report an error on username',(done)=>{
		const req = request.post('user/create')
		req.send({
			username:'test1',
			password:'1111',
			email:'f1@gmail.com',
			admin:false
		})
		req.end(function (err,res) {
			if(err) return console.log(err)
			
			expect(JSON.parse(res.text).message.msg)
			.toEqual('Username taken by another user')
			return done()
		})	
			  
	}),
	it('should report an error on email',(done)=>{
		const req = request.post('user/create')
		req.send({
			username:'test4',
			password:'1111',
			email:'test1@gmail.com',
			admin:false
		})
		req.end(function (err,res) {
			if(err) return console.log(err)
			
			expect(JSON.parse(res.text).message.msg)
			.toEqual('Email taken by another user')
			return done()
		})	
			  
	})


})