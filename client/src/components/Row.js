import React from 'react';
import {Link } from 'react-router-dom'
const Row = (props)=>(
    <tr >
    <td>{props.id}</td>
    <td>{props.username}</td>
    <td>{props.email}</td>
    <td><p>
    <Link to={props.to}><button type="button"
      className="btn btn-info btn-sm">
      <span className="glyphicon glyphicon-eye-open"
       aria-hidden="true"></span>
      </button></Link>
    {" "}
    <button onClick={(e)=>props.click(e,props)} type="button"
      className="btn btn-danger btn-sm">
      <span className="glyphicon glyphicon-trash"
       aria-hidden="true"></span>
      </button>
      </p>
    </td>
    </tr>


  )
export default Row
