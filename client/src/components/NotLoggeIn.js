import React, { Component } from 'react';
import { Link } from 'react-router-dom'

const NotLoggeIn =(props) =>(
    <ul className="nav navbar-nav navbar-right">
        <li><Link to='/signup'>Register</Link></li>
    </ul>
)
export default NotLoggeIn;
