import React, { Component } from 'react';


const UserLoggedIn =(props) =>{
  return (
    <ul className="nav navbar-nav navbar-right">
        <li><a>Hello {props.user}</a></li>
        <li><a href='' onClick={props.click}>Logout</a></li>
    </ul>

  )


}
export default UserLoggedIn;
