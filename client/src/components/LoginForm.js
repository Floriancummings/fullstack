import React, { Component } from 'react';
import TextField from './TextField';
import Submit from './Submit';
import ErrorMsg from './ErrorMsg';
import { Link } from 'react-router-dom'
const LoginForm = (props)=>(
	<div className="auth-form col-md-4 col-md-offset-4">
    <h1>LOGIN</h1>
    <hr/>
    <form onSubmit={props.handleSubmit}>
       <TextField
        value={props.loginDetails.username}
        handleChange={props.handleChange}
        type="text"
        name="username"
        placeholder="Username"
        title='Username'
        />
       <TextField
         value={props.loginDetails.password}
         handleChange={props.handleChange}
         type="password"
         name="password"
         placeholder="Password"
         title='Password'
        />
        {props.loginDetails.msg ?<ErrorMsg msg={props.loginDetails.msg.msg} /> :''}
      <Submit title='Login' />
    </form>
    <span><small className='pull-right'>Dont have an account? <Link to='/signup'>Register</Link></small></span>
  </div>

	)
export default LoginForm;