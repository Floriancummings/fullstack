import React,{ Component } from 'react';
import {Link } from 'react-router-dom'
import Row from './Row';
import {withRouter} from 'react-router-dom';
class Table extends Component{

  render(){
    return(
      <table className ='table'>
        <thead>
          <tr>
            <th>id</th>
            <th>Username</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          {this.props.users.map((user,i)=><Row click={this.props.click} to={`${this.props.match.url}/${user.id}`}
          key={i} {...user}/>)}
        </tbody>
      </table>


    )
  }


}
export default withRouter(Table)
