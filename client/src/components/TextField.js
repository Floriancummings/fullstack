import React from 'react';

const TextField = (props)=>(
    <div className="form-group"> <label>{props.title}</label>
      <input value={props.value} onChange={props.handleChange}
      type={props.type} name={props.name}
      className="form-control" placeholder={props.placeholder} required/>
    </div>


  )
export default TextField
