import React, { Component } from 'react';
import TextField from './TextField';
import ErrorMsg from './ErrorMsg';
import { Link } from 'react-router-dom';
import Submit from './Submit';
const SignUpForm =(props)=> {
		return (
			<div className="auth-form col-md-4 col-md-offset-4">
      <h1>Register</h1>
      <hr/>
          <form onSubmit={props.handleSubmit}>
            <TextField
              title='Email address'
              type='email'
              name='email'
              handleChange ={props.handleChange}
              value ={props.regdetails.email}
              placeholder="Email"
            />
            {props.regdetails.msg &&
              props.regdetails.msg.type ==='email'
               ?<ErrorMsg msg={props.regdetails.msg.msg} /> :''}
            <TextField
              value={props.regdetails.username}
              type="text"
              name='username'
              handleChange={props.handleChange}
              title='Username'
              placeholder='Username'
            />
            {props.regdetails.msg &&
              props.regdetails.msg.type ==='username'
               ?<ErrorMsg msg={props.regdetails.msg.msg} /> :''}
            <TextField
              value={props.regdetails.password}
              type="password"
              name='password'
              handleChange={props.handleChange}
              title='Password'
              placeholder='Password'
            />
             
            <Submit title='CREATE AN ACCOUNT' />
          </form>
          <span><small className='pull-right'>Already have an account? <Link to='/' > Login</Link></small></span>
        </div>				
		);
	
}

export default SignUpForm;