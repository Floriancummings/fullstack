import React, { Component } from 'react';
import Navbar from '../containers/Navbar';
import Profile from '../containers/Profile';
import SignUp from '../containers/SignUp';
import Login from '../containers/Login';
import Admin from '../containers/Admin';
import {
  Switch,
  Route,
  Redirect,
  withRouter
} from 'react-router-dom'
import '../App.css';

class App extends Component {
  render() {
    return (
      <div className='app'>
        <Navbar />
        <Switch>
          <Route  exact path='/' component={Login} />
          <Route  path='/profile' component={Profile} />
          <Route  path='/signup' component={SignUp} />
          <Route  path='/admin' component={Admin} />
        </Switch>
      </div>
    );
  }
}

export default App;
