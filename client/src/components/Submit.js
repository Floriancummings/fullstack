
import React from 'react';

const Submit = (props)=>(
    <button type="submit" name="button"
    className="form-control btn btn-primary">{props.title}</button>
  )
export default Submit;
