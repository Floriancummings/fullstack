import React from 'react'

import reducers from './reducers';
import Login from './containers/Login';
import { createStore } from 'redux';
import {loggedIn} from './actions'
import renderer from 'react-test-renderer';
import Shallow from 'react-test-renderer/shallow';
import supertest from 'supertest';
const store = createStore(reducers);
const request = supertest('http://localhost:1337/')
describe('Login ',()=>{
	it('should authenticate a user in',(done)=>{
		const req = request.post('user/login')
		req.send({username:'test1',password:'1111'})
		req.end(function (err,res) {
			if(err) return console.log(err)
			if(JSON.parse(res.text).success)
			store.dispatch(loggedIn(JSON.parse(res.text).user));
			expect(store.getState().login.showLoginPage)
			.toEqual(false)
			return done()
		})	
			  
	}),
	it('should report invalid login details',(done)=>{
		const req = request.post('user/login')
		req.send({username:'fail',password:'1111'})
		req.end(function (err,res) {
			if(err) return console.log(err)
			
			expect(JSON.parse(res.text).message.msg)
			.toEqual('invalid username or password')
			return done()
		})	
			  
	})


})