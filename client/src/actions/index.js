export const handleChange = (e) =>(
  {
    type:'ON_CHANGE',
    payload:e.target
  }
)
export const onLoad = () =>({
  type:'LOAD'
})
export const loggedIn = (payload,token)=>({
  type:'LOGGED_IN',
  payload,token
})
export const registered = (payload)=>({
  type:'REGISTERED',
  payload
})
export const receiveData = (payload)=>({
  type:'RECEIVE_DATA',
  payload
})
export const loginPage = ()=>({
  type:'LOGIN_PAGE',
  
})
export const profilePage = ()=>({
  type:'PROFILE_PAGE',
  
})
export const signUpPage =()=>({
  type:'REG_PAGE'
})
export const Delete =(payload)=>({
  type:'DELETE',
  payload
})
export const onError = (payload) =>({
  type:'ERROR',
  payload
})
export const viewUser =(payload) =>({
  type:'VIEW',
  payload
})
export const editting = (payload) =>({
  type:'EDIT',
  payload
})
export const showLogin = () =>({
  type:'SHOW_LOGIN',
  
})
export const showProfile = (payload) =>({
  type:'SHOW_PROFILE',
  payload
})
export const logout = () =>({
  type:'LOGGED_OUT',
  
})
export const showReg = () =>({
  type:'SHOW_REG',
  
})
export const showAdmin = () =>({
  type:'SHOW_ADMIN',
  
})
export const adminPage = () =>({
  type:'ADDMIN_PAGE',
  
})
